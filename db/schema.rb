# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160519164318) do

  create_table "cell_types", force: :cascade do |t|
    t.text     "desc",            limit: 65535
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "species_id",      limit: 4
    t.integer  "sample_class_id", limit: 4
  end

  add_index "cell_types", ["sample_class_id"], name: "index_cell_types_on_sample_class_id", using: :btree
  add_index "cell_types", ["species_id"], name: "index_cell_types_on_species_id", using: :btree

  create_table "expressions", force: :cascade do |t|
    t.integer  "transcript_id", limit: 4
    t.integer  "cell_type_id",  limit: 4
    t.float    "value",         limit: 24
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "expressions", ["cell_type_id"], name: "index_expressions_on_cell_type_id", using: :btree
  add_index "expressions", ["transcript_id"], name: "index_expressions_on_transcript_id", using: :btree

  create_table "sample_classes", force: :cascade do |t|
    t.text     "desc",       limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "species", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "species", ["name"], name: "index_species_on_name", unique: true, using: :btree

  create_table "submits", force: :cascade do |t|
    t.string   "name",         limit: 255
    t.text     "msg",          limit: 65535
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "email",        limit: 255
    t.integer  "status",       limit: 4
    t.text     "error_reason", limit: 65535
  end

  add_index "submits", ["name"], name: "index_submits_on_name", unique: true, using: :btree

  create_table "transcripts", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.integer  "submit_id",  limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "transcripts", ["name"], name: "index_transcripts_on_name", using: :btree
  add_index "transcripts", ["submit_id"], name: "index_transcripts_on_submit_id", using: :btree

  add_foreign_key "cell_types", "sample_classes"
  add_foreign_key "cell_types", "species"
  add_foreign_key "expressions", "cell_types"
  add_foreign_key "expressions", "transcripts"
  add_foreign_key "transcripts", "submits"
end
