class CreateCellTypes < ActiveRecord::Migration
  def change
    create_table :cell_types do |t|
      t.text :desc

      t.timestamps null: false
    end
  end
end
