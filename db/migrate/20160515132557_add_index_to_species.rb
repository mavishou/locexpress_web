class AddIndexToSpecies < ActiveRecord::Migration
  def change
    add_index :species, :name, unique: true
  end
end
