class CreateSubmits < ActiveRecord::Migration
  def change
    create_table :submits do |t|
      t.string :name
      t.integer :status
      t.text :msg

      t.timestamps null: false
    end
  end
end
