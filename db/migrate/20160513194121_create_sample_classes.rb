class CreateSampleClasses < ActiveRecord::Migration
  def change
    create_table :sample_classes do |t|
      t.text :desc

      t.timestamps null: false
    end
  end
end
