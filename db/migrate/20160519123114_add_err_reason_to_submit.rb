class AddErrReasonToSubmit < ActiveRecord::Migration
  def change
    add_column :submits, :error_reason, :text
  end
end
