class AddIndexToTranscripts < ActiveRecord::Migration
  def change
    add_index :transcripts, :name, unique: true
  end
end
