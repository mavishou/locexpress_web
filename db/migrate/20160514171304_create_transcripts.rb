class CreateTranscripts < ActiveRecord::Migration
  def change
    create_table :transcripts do |t|
      t.string :name
      t.references :submit, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
