class AddSpeciesToCellTypes < ActiveRecord::Migration
  def change
    add_reference :cell_types, :species, index: true, foreign_key: true
  end
end
