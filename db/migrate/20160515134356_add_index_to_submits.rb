class AddIndexToSubmits < ActiveRecord::Migration
  def change
    add_index :submits, :name, unique: true
  end
end
