class RemoveUniqueConstrainOfTranscripts < ActiveRecord::Migration
  def change
    remove_index :transcripts, :name
    add_index :transcripts, :name
  end
end
