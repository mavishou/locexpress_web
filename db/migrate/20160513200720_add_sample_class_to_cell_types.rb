class AddSampleClassToCellTypes < ActiveRecord::Migration
  def change
    add_reference :cell_types, :sample_class, index: true, foreign_key: true
  end
end
