class AddEmailToSubmits < ActiveRecord::Migration
  def change
    add_column :submits, :email, :string
  end
end
