class CreateExpressions < ActiveRecord::Migration
  def change
    create_table :expressions do |t|
      t.references :transcript, index: true, foreign_key: true
      t.references :cell_type, index: true, foreign_key: true
      t.float :value

      t.timestamps null: false
    end
  end
end
