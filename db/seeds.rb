# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

def import_species
  puts 'Importing species...'
  Species.create(name: 'Human')
  Species.create(name: 'Mouse')
end


def import_sample_class
  puts 'Importing sample classes'
  SampleClass.create(desc: 'Normal Tissues / Cells')
  SampleClass.create(desc: 'Cell Lines')
end


def import_cell_type_info
  puts 'Importing cell type info...'

  require 'csv'
  CSV.foreach('lib/assets/cell_types.txt',
              col_sep: "\t",
              headers: true) do |row|
    CellType.create(
        species_id: row['species_id'],
        sample_class_id: row['sample_class_id'],
        desc: row['desc'])
  end
end

# def import_status_info
#   puts 'Importing status info...'
#   Status.create(id: -2, desc: 'waiting')
#   Status.create(id: -1, desc: 'running')
#   Status.create(id: 0, desc: 'success')
#   Status.create(id: 1, desc: 'failed due to user')
#   Status.create(id: 2, desc: 'failed due to system')
# end

import_species
import_sample_class
import_cell_type_info
# import_status_info