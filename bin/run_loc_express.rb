#!/usr/bin/ruby

def get_exp_file(submit_name)
  File.join(ENV['RUN_DIR'], submit_name, 'merged_exp.txt')
end

def insert_exp_into_db(submit)
  # read the exp file
  exp_file = get_exp_file(submit.name)
  exp_hash = Hash.new {|hsh, key| hsh[key] = {} }
  require 'csv'
  CSV.foreach(exp_file,
              col_sep: "\t",
              headers: true) do |row|
    exp_hash[row['transcript_id']][Integer(row['cell_type'])] = Float(row['fpkm'])
  end
  # insert into db
  exp_hash.each do |transcript_name, cell_type_exp_hash|
    transcript = Transcript.create(name: transcript_name, submit_id: submit.id)
    cell_type_exp_hash.each do |cell_type, exp|
      Expression.create(transcript_id: transcript.id,
                        cell_type_id: cell_type,
                        value: exp)
    end
  end
end

# get parameters
submit_id = Integer(ARGV[0])
species = ARGV[1]

# before running #
submit = Submit.find(submit_id)

# run #
submit.update(status: -1)
puts "LocExpess is running for #{submit.name}..."
begin
  script_to_run = File.join(ENV['SCRIPT_DIR'], 'loc_express.py')
    run_cmd = "python #{script_to_run} --run-id=#{submit.name} -s #{species} -e #{ENV['RUN_MODE']}"
  puts run_cmd
  run_output = %x(#{run_cmd})
rescue
  submit.update(status: 3,
                msg: 'System error',
                error_reason: 'Ruby run error')
  exit
end


# after running #
run_info = ActiveSupport::JSON.decode(run_output)
exit_code = run_info['exit_code']
submit.update(msg: run_info['error_msg'],
              error_reason: run_info['error_reason'])

if exit_code == 0 # success
  insert_exp_into_db(submit)
end
submit.update(status: exit_code)

