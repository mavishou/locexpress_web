class RunLocExpressJob < ActiveJob::Base
  queue_as :default

  def perform(submit)
    submit.update(status: -1)
    sleep(20)
    submit.update(status: 0)
  end
end
