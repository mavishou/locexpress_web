class DownloadController < ApplicationController
  def show
    submit_name = params[:name]
    file_path_to_download = get_file_path_to_download(submit_name)
    send_file(file_path_to_download)
  end

  private

  def get_file_path_to_download(submit_name)
    file_name = "LocExpress_#{submit_name}.zip"
    File.join(ENV['RUN_DIR'], submit_name, file_name)
  end

end
