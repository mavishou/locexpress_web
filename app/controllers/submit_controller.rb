
class SubmitController < ApplicationController
  skip_before_action :verify_authenticity_token

  def new
    @cell_types_info = get_cell_type_info
  end

  def create
    # check cell types
    cell_types = check_cell_types(params[:cell_types])
    if cell_types == 'NO'
      return redirect_to root_path
    end

    # check species
    species_ids = get_species_ids(cell_types)
    species = check_species_uniq(species_ids)
    if species == 'NO'
      return redirect_to new_submit_path
    end

    @submit = Submit.create(name: generate_submit_name, status: -2)

    # write user submit content
    user_dir = prepare_user_dir(@submit.name)
    write_cell_types(cell_types, user_dir)

    if params.has_key?(:gtf_file)
      uploaded_io = params[:gtf_file]
      write_user_submit_file(uploaded_io, user_dir)
    elsif params.has_key?(:input_gtf_text)
      user_submit_text = params[:input_gtf_text]
      write_user_submit_text(user_submit_text, user_dir)
    else
      flash[:danger] = 'Invalid option!'
      return redirect_to new_submit_path
    end

    # run LocExpress
    run_loc_express(@submit, species)
    redirect_to submit_path(@submit.name)
  end

  def show
    @submit = Submit.where(name: params[:name]).first
    @cell_type_hash, @total_exp_hash = get_exp_and_cell_types(@submit)
  end

  # for ajax
  def empty_cell_type
    respond_to do |format|
      format.js { render :layout => false }
    end
  end


  # helper functions
  private

  def get_cell_type_info
    cell_types_info = []
    Species.order(:id).each do |species|
      sample_classes = []
      species.sample_classes.order(:id).distinct.each do |sample_class|
        cell_types = []
        CellType.where(species_id: species.id, sample_class_id: sample_class.id).each do |cell_type|
          cell_types.push([cell_type.id, cell_type.desc])
        end
        sample_classes.push({sample_class.desc => cell_types})
      end
      cell_types_info.push({species.name => sample_classes})
    end
    return cell_types_info
  end

  def get_species_ids(cell_types)
    CellType.select(:species_id).where("id in (#{cell_types.join(',')})").distinct.pluck(:species_id)
  end

  def check_species_uniq(species_ids)
    if species_ids.length > 1
      flash[:danger] = 'Sorry, you should only select cell types in one species.'
      return 'NO'
    else
      Species.find(species_ids).first.name
    end
  end

  def generate_submit_name
    name_exists = TRUE
    while name_exists do
      submit_name = SecureRandom.urlsafe_base64
      if not Submit.exists?(name: submit_name)
        name_exists = FALSE
        break
      end
    end
    return submit_name
  end

  def check_cell_types(raw_cell_types)
    cell_types = []
    raw_cell_types.each do |c|
      if c =~ /^\d+$/
        cell_types.push(c)
      end
    end
    if cell_types.length == 0
      flash[:danger] = 'Invalid option!'
      return 'NO'
    else
      return cell_types
    end
  end

  def get_user_dir(submit_name)
    File.join(ENV['RUN_DIR'], submit_name)
  end

  def prepare_user_dir(submit_name)
    user_dir = get_user_dir(submit_name)
    Dir.mkdir(user_dir) if not File.exists?(user_dir)
    return user_dir
  end

  def write_cell_types(cell_types, run_dir)
    output_file = File.join(run_dir, 'cell_type.list')
    File.open(output_file, 'w') {|f| f.puts(cell_types) }
  end

  def write_user_submit_text(input_gtf_text, run_dir)
    output_file = File.join(run_dir, 'input.gtf')
    File.open(output_file, 'w') {|f| f.write(input_gtf_text) }
  end

  def write_user_submit_file(uploaded_io, run_dir)
    output_file = File.join(run_dir, 'input.gtf')
    File.open(output_file, 'wb') {|f| f.write(uploaded_io.read) }
  end

  def run_loc_express(submit, species)
    user_dir = get_user_dir(submit.name)
    bin_dir = File.join(Rails.root, 'bin')
    script = File.join(bin_dir, 'run_loc_express.rb')
    log_file = File.join(user_dir, 'ruby_run.log')
    run_cmd = "#{bin_dir}/rails runner -e #{Rails.env} #{script} #{submit.id} #{species.downcase!} >#{log_file} 2>&1 &"
    puts run_cmd
    %x(#{run_cmd})
  end

  def get_exp_and_cell_types(submit)
    cell_type_desc_hash = Hash.new {|hsh, key| hsh[key] = Set[]}
    total_exp_hash = Hash.new {|hsh, key| hsh[key] =
        Hash.new {|hsh2, key2| hsh2[key2] = []}}
    exp = Submit.get_exp(submit.id)
    exp.each do |row|
      cell_type_desc_hash[row.sample_class_desc].add(row.cell_type_desc)
      total_exp_hash[row.trans_name][row.sample_class_desc].push(row.value)
    end
    return cell_type_desc_hash, total_exp_hash
  end

end
