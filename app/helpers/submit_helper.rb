module SubmitHelper
  def get_run_status_info(submit_status)

    if submit_status == 0
      return 'Successfully finished', 'success'
    elsif submit_status == -2
      return 'Waiting', 'warning'
    elsif submit_status == -1
      return 'Running', 'info'
    else
      return 'Failed', 'danger'
    end
  end

  def get_chart_width(cell_type_num)
    chart_width = 100 * cell_type_num
    min_width = 500
    max_width = 1100
    if chart_width <= min_width
      chart_width = min_width
    elsif chart_width >= max_width
      chart_width = max_width
    end
    return chart_width
  end

end
