class Transcript < ActiveRecord::Base
  belongs_to :submit
  has_many :cell_types, through: :expressions
  has_many :expressions
  validates :name, presence: true
end
