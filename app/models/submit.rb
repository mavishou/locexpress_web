class Submit < ActiveRecord::Base
  has_many :transcripts
  has_many :cell_types, through: :expressions
  has_many :expressions, through: :transcripts
  validates :name, presence: true, uniqueness: true
  scope :get_exp, ->(submit_id) { Expression.joins(:transcript, {cell_type: :sample_class}).
      where('transcripts.submit_id = ?', submit_id).
      order('transcripts.id, sample_classes.id, cell_types.id').
      select("transcripts.name as trans_name,
              sample_classes.desc as sample_class_desc,
              cell_types.desc as cell_type_desc,
              expressions.value as value") }
end
