class Expression < ActiveRecord::Base
  belongs_to :transcript
  belongs_to :cell_type
  has_one :submit, through: :transaction
  validates :value, presence: true
end
