class Species < ActiveRecord::Base
  has_many :cell_types
  has_many :sample_classes, through: :cell_types
  validates :name, presence: true, uniqueness: true
end
