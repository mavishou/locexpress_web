class SampleClass < ActiveRecord::Base
  has_many :cell_types
  validates :desc, presence: true, uniqueness: true
end
