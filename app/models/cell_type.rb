class CellType < ActiveRecord::Base
  belongs_to :species
  belongs_to :sample_class
  validates :desc, presence: true
  validates_uniqueness_of :desc, scope: [:species_id]
  scope :info, -> {CellType.joins(:species, :sample_class).
      order('species.id, sample_classes.id, cell_types.id').
      select('species.name as species_name,
              sample_classes.desc as sample_class_desc,
              cell_type.id
              cell_types.desc')}
end
