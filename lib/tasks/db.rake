namespace :db do
  desc "Import cell type table to the cell_types database"
  task create_species: :environment do
    Species.create(name: 'human')
    Species.create(name: 'mouse')
  end

  task create_sample_classes: :environment do
    SampleClass.create(desc: 'Normal Tissues / Cells')
  end

  task import_cell_type: :environment do

  end

end
